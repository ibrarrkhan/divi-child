<?php
  $publishable_key = 'pk_test_IvVEHTVckThBV8GhHSbVhMBv'; // get it in Account Settings -> API Keys
  $plugin_name = "Test Stripe"; // plugin title
  $plugin_image_url = wp_get_attachment_url( get_post_thumbnail_id( $plugin_id ) ); // image URL, it may be your site logo as well
  $plugin_price = 500; // if currency is USD (default) the price should be in cents then
?>
<div class="strip-button">
  <form action="<?php echo get_stylesheet_directory_uri() ?>/stripe/charge.php" method="post">
    <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="<?php echo $publishable_key; ?>"
    data-name="<?php echo $plugin_name; ?>"
    data-description="By Misha Rudrastyh"
    data-image="<?php echo $plugin_image_url; ?>"
    data-amount="<?php echo $plugin_price; ?>"
    data-label='Checkout'
    data-locale="auto"></script>
    <?php /* you can pass parameters to php file in hidden fields, for example - plugin ID */ ?>
    <input type="hidden" name="plugin_id" value="<?php echo $plugin_id; ?>">
  </form>
</div>
<script>
// document.getElementsByClassName("stripe-button-el")[0].disabled=true;
</script>
