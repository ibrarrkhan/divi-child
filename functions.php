<?php

opcache_invalidate(__FILE__, true);


function my_theme_enqueue_styles() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function repeater_script() {
    wp_enqueue_script( 'repeater', get_stylesheet_directory_uri() . '/assets/js/repeater.js', array( 'jquery' ),'',true );
}
add_action( 'wp_enqueue_scripts', 'repeater_script' );

function add_custom_style(){
    // include (get_stylesheet_directory() . '/customstyle.php');
}
add_action('wp_head', 'add_custom_style');

function about_us( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/aboutus/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
	return $events->aboutus;    
}
add_shortcode('aboutpage', 'about_us');

function agenda( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/sessions/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ob_start();
    $first = true;
    foreach ( $events as $event_key => $event_value ) {
        $date = $event_value->startTime;
        $fixed = date('g:i A', strtotime($date));
        if ( $first ) {
            ?>
            <div class="et_pb_module et_pb_text hover_circle_shedule_after et_pb_bg_layout_light et_pb_text_align_left">
                <div class="et_pb_text_inner"><p style="padding-top: 7px;"><strong><?php echo $fixed?> – <?php echo $event_value->name; ?></strong></p></div>
			</div>
             <?php
            $first = false;
        }
        elseif (!empty($event_value->detailedDescription)) {
            ?>
            <div class="et_pb_module et_pb_toggle et_pb_toggle_0 hover_circle_shedule et_pb_toggle_item et_pb_toggle_close">
                <h5 class="et_pb_toggle_title"><?php echo $fixed?> – <?php echo $event_value->name; ?></h5>
                <div class="et_pb_toggle_content clearfix" style="display: none;">
                    <?php echo $event_value->detailedDescription?>
                </div>
            </div>
            <?php
        }
        else {
            ?>
            <div class="et_pb_module et_pb_text hover_circle_shedule et_pb_bg_layout_light  et_pb_text_align_left">
                <div class="et_pb_text_inner"><p style="padding-top: 7px;"><strong><?php echo $fixed?> – <?php echo $event_value->name; ?></strong></p></div>
			</div>
            <?php
        }
    }
    $output = ob_get_clean();
    return $output;
    }
add_shortcode('agenda', 'agenda');

function award_category( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/award-category/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
    <?php
    ob_start();
    ?>
    <div class="row">
    <?php
    foreach ( $events as $event_key ) {
        ?>
        <div class="col-lg-4" style="margin-bottom: 20px;">
        <div class="et_pb_module et_pb_text et_pb_text_1 award-cat-box events-block et_pb_bg_layout_light et_pb_text_align_left">
            <div class="et_pb_text_inner"><h3><?php echo $event_key->name?></h3><span class="award-key" style="display:none;"><?php echo $event_key->awardcategoryid;?></span><p><?php echo $event_key->description?></p></div>
        </div>
        </div>
        <?php
    }
    ?>
    </div>
    <?php
    include(get_stylesheet_directory() . '/forms/nominee-form.php');
    ?>
    <script>
    jQuery(document).ready(function(){
        jQuery('.events-block').click(function(){
            jQuery('.message').hide();
            jQuery('.main-form-style').show( "slow" );
            var title = jQuery(this).find('h3').text();
            var key = jQuery(this).find('span').text();
            jQuery('.form-title').html('<h3>Nominate for '+title+'</h3>');
            jQuery('#award_category').val(key);
//            jQuery('html, body').animate({ scrollTop: jQuery("#nominee_details_form").offset().top - 200}, 1000);
        });
    })
    </script>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('awardcategory', 'award_category');

function register_event( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/passes/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ob_start();
    require_once (get_stylesheet_directory() . '/inc/stripe-php/init.php');
    
    $total = 1;
    
    foreach ($events as $key => $value) {
        if(  $value->customFields->slgc_externalpass == true) {
            $total++;
        }
    }
//    $total = count($events)+1;
    ?>
    <select name="select-pass" id="pass_dropdown" size="<?php echo $total; ?>">
<!--    <select name="select-pass" id="pass_dropdown">-->
        <option value="-1">Select a pass</option>
        <?php foreach ($events as $key => $value): ?>
        <?php if(  $value->customFields->slgc_externalpass == true): ?>	
        <option data-seat="<?php echo $value->customFields->slgc_size; ?>" data-seat-available="<?php echo $value->customFields->slgc_size; ?>" value="<?php echo $value->customFields->slgc_productnumber; ?>" data-price="<?php  echo $value->price?>" data-eventId="<?php  echo $value->eventId ?>" data-passid="<?php  echo $value->passId ?>">  <?php  echo $value->passName?></option>
    <?php  endif; ?>
        <?php endforeach ?>
        </select>
<!--
    <select name="select-pass" id="pass_dropdown">
        <option value="-1">Select a pass</option>
        <option data-seat="10" data-seat-available="10" value="suppliers-10" data-price="1750" data-eventId="ae546c9f-50d4-e911-a836-000d3a289110"> Suppliers – Table of 10 Awards (£1750.0000) </option>
        <option data-seat="10" data-seat-available="10" value="small-business-10" data-price="1500" data-eventId="ae546c9f-50d4-e911-a836-000d3a289110"> Small Businesses – Table of 10 Awards (£1500.0000) </option>
        <option data-seat="2" data-seat-available="2" value="small-business-2" data-price="500" data-eventId="ae546c9f-50d4-e911-a836-000d3a289110"> Small Businesses - Table of 2 Awards (£500.0000) </option>
    </select>
-->
    <?php
    include(get_stylesheet_directory() . '/forms/select-pass-form.php');
    ?>
    <script>
    jQuery(document).ready(function(){

      jQuery('#pass_dropdown').change(function() {
                    jQuery("#passid").val( this.value  );
                    var element = jQuery(this).find('option:selected'); 
                    var eventId = element.attr("data-eventId"); 
                    jQuery('#eventId').val(eventId); 

                    var passId = element.attr("data-passid"); 
                    jQuery('#eventPassId').val(passId); 

          if (this.value != "-1") {
                    jQuery(".attendee-number").html(jQuery(this).find(':selected').attr('data-seat-available')-1);
                    jQuery(".registration_price").val(jQuery(this).find(':selected').attr('data-price'));
                    jQuery(".event-id").val(jQuery(this).find(':selected').attr('data-eventId'));
                    jQuery('#repeater-container').show();


                     var attendeeNumber = jQuery(".attendee-number").html();

                     // alert( attendeeNumber);
                      if (attendeeNumber == 0) {
                        jQuery(".repeater-add-btn").attr("disabled", true);
                      }else{
                        jQuery(".repeater-add-btn").attr("disabled", false);

                      }

                    jQuery("#repeater_form").validate({
                            ignore: [],
                            rules: {
                                'email[]': {
                                        required: true,
                                        email: true
                                },
                                'first_name[]': {
                                    required: true
                                },
                                'last_name[]': {
                                    required: true
                                }
                            },
                            messages: {
                                'email[]': "Valid email is required",
                                'first_name[]': {
                                        required: "First name is required",
                                },
                                'last_name[]': {
                                    required: "Last name is required",
                                }
                            },
                            submitHandler: function(form) { // <- pass 'form' argument in
                                jQuery('#repeater_form button.stripe-button-el').prop('disabled', 'disabled');
                                form.submit(); // <- use 'form' argument here.
                            }
                    });
          }
          else {
            jQuery('#repeater-container').hide();
          }
      });
    })
</script>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('register', 'register_event');

function judges_list( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/judges/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
    <?php
    ob_start();
    ?>
    <div class="row">
    <?php
    foreach ( $events as $event_key => $event_key_value ) {
        ?>
        <div class="col-lg-4">
            <div class="et_pb_module et_pb_team_member et_pb_team_member_0 judges_picture et_hover_enabled et_pb_bg_layout_light clearfix ">
                <div class="et_pb_team_member_image et-waypoint et_pb_animation_off et-animated"><img style="min-width: 100%;" src="<?php echo 'data:image/png;base64, '.$event_key_value->image ?>" alt="Steve Distante" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 559px, 100vw"></div>
                <div class="et_pb_team_member_description">
                    <h3 class="et_pb_module_header" style="font-weight:bold;"><?php echo $event_key_value->name?></h3>
                    <p class="et_pb_member_position"><?php echo $event_key_value->jobTitle?>, <?php echo $event_key_value->company?></p>
                    <div>
                        <div style="border: 1px solid #333; width: 15%;"></div>
                        <div style="padding-top: 5px !important;"></div>
                    </div>
                </div>
            </div>
            <div class="et_pb_module et_pb_text et_pb_text_1 judges_text et_pb_bg_layout_light  et_pb_text_align_left">

                <div class="et_pb_text_inner">
                    <p><?php echo $event_key_value->description?></p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('judges', 'judges_list');

function speakers_list( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/speaker/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
    <?php
    ob_start();
    ?>
    <div class="row">
    <?php
    foreach ( $events as $event_key => $event_key_value ) {
        ?>
        <div class="col-lg-4">
            <div class="et_pb_module et_pb_team_member et_pb_team_member_0 judges_picture et_hover_enabled et_pb_bg_layout_light clearfix ">
                <div class="et_pb_team_member_image et-waypoint et_pb_animation_off et-animated"><img style="min-width: 100%;" src="<?php echo 'data:image/png;base64, '.$event_key_value->image ?>" alt="Steve Distante" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 559px, 100vw"></div>
                <div class="et_pb_team_member_description">
                    <h3 class="et_pb_module_header" style="font-weight:bold;"><?php echo $event_key_value->name?></h3>
                    <p class="et_pb_member_position"><?php echo $event_key_value->jobTitle?>, <?php echo $event_key_value->company?></p>
                    <div>
                        <div style="border: 1px solid #333; width: 15%;"></div>
                        <div style="padding-top: 5px !important;"></div>
                    </div>
                </div>
            </div>
            <div class="et_pb_module et_pb_text et_pb_text_1 judges_text et_pb_bg_layout_light  et_pb_text_align_left">

                <div class="et_pb_text_inner">
                    <p><?php echo $event_key_value->description?></p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('speakers', 'speakers_list');

function advisory_board( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/advisoryboard/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
    <?php
    ob_start();
    ?>
    <div class="row">
    <?php
    foreach ( $events as $event_key => $event_key_value ) {
        ?>
        <div class="col-lg-4">
            <div class="et_pb_module et_pb_team_member et_pb_team_member_0 judges_picture et_hover_enabled et_pb_bg_layout_light clearfix " style="border-radius: 20px 20px 20px 20px !important;">
                <div class="et_pb_team_member_image et-waypoint et_pb_animation_off et-animated"><img style="min-width: 100%;" src="<?php echo 'data:image/png;base64, '.$event_key_value->image ?>" alt="Steve Distante" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 559px, 100vw"></div>
                <div class="et_pb_team_member_description">
                    <h3 class="et_pb_module_header" style="font-weight:bold;"><?php echo $event_key_value->advisorName?></h3>
                    <p class="et_pb_member_position"><?php echo $event_key_value->jobTitle?>, <?php echo $event_key_value->company?></p>
                    <div>
                        <div style="border: 1px solid #333; width: 15%;"></div>
                        <div style="padding-top: 5px !important;"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('advisoryboard', 'advisory_board');

function sponsors_list( $atts ) {
    $request = wp_remote_get( "https://". get_option('event_portal') .".microsoftcrmportals.com/api/events/sponsor/?readableEventId=".$atts['eventid']);
    $events = json_decode( wp_remote_retrieve_body( $request ) );
    extract(shortcode_atts(array(
        'type' => ''
      ), $atts));
    ?>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
    <?php
    $color = get_option('accent_color');
    ob_start();
    ?>
    <div class="row">
    <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_center">
        <div class="et_pb_text_inner">
            <div style="display: table;">
                <h1 style="text-align: center; margin: 0px;"><?php echo $type ?></h1>
                <p class="title-bottom-border" style="padding: 0;"></p>
            </div>
        </div>
    </div>
    <?php
    foreach ( $events as $event_key => $event_key_value ) {
          if($type == $event_key_value->sponsorshipCategory){
            ?>
            <div class="col-12 sponsor_container">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">
                    <div class="et_pb_module et_pb_blurb et_pb_blurb_0 sponsor_float et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
                        <div class="et_pb_blurb_content">
                            <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><img style="min-width: 100%;" src="<?php echo 'data:image/png;base64, '.$event_key_value->image ?>" class="et-waypoint et_pb_animation_top et-animated"></span></div>
                            <div class="et_pb_blurb_container">
                                <h3 class="et_pb_module_header"><span><?php echo $event_key_value->sponsorName?></span></h3>
                                <div class="et_pb_blurb_description">
                                    <p><span><?php echo $event_key_value->description?></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
          } else { }
    }
    ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('sponsors', 'sponsors_list');

// Setting a custom timeout value for cURL. Using a high value for priority to ensure the function runs after any other added to the same action hook.
add_action('http_api_curl', 'sar_custom_curl_timeout', 9999, 1);
function sar_custom_curl_timeout( $handle ){
	curl_setopt( $handle, CURLOPT_CONNECTTIMEOUT, 30 ); // 30 seconds. Too much for production, only for testing.
	curl_setopt( $handle, CURLOPT_TIMEOUT, 30 ); // 30 seconds. Too much for production, only for testing.
}

// Setting custom timeout for the HTTP request
add_filter( 'http_request_timeout', 'sar_custom_http_request_timeout', 9999 );
function sar_custom_http_request_timeout( $timeout_value ) {
	return 30; // 30 seconds. Too much for production, only for testing.
}

// Setting custom timeout in HTTP request args
add_filter('http_request_args', 'sar_custom_http_request_args', 9999, 1);
function sar_custom_http_request_args( $r ){
	$r['timeout'] = 30; // 30 seconds. Too much for production, only for testing.
	return $r;
}


function add_theme_menu_item() {
        add_menu_page("Event Settings", "Event Settings", "manage_options", "theme-settings", "theme_settings_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");

function theme_settings_page() {
    ?>
	    <div class="wrap">
	    <h1>Customize Event Theme</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("event_settings_all");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}

function main_accent_color() {
	?>
    	<input type="text" name="accent_color" placeholder="Enter Hexcode" id="accent_color" value="<?php echo get_option('accent_color'); ?>" />
    <?php
}

function button_hover_color() {
	?>
    	<input type="text" name="hover_color" placeholder="Enter Hexcode" id="hover_color" value="<?php echo get_option('hover_color'); ?>" />
    <?php
}

function main_header_image() {
	?>
    	<input type="URL" name="header_image" id="header_image" value="<?php echo get_option('header_image'); ?>" />
    <?php
}

function bottomcta_cta_text() {
	?>
    	<input type="text" name="bottom_cta_text" id="bottom_cta_text" value="<?php echo get_option('bottom_cta_text'); ?>" />
    <?php
}

function bottomcta_body_text() {
	?>
    	<input type="text" name="bottom_body_text" id="bottom_body_text" value="<?php echo get_option('bottom_body_text'); ?>" >
    <?php
}

function bottomcta_background_image() {
	?>
    	<input type="URL" name="bottom_background_image" id="bottom_background_image" value="<?php echo get_option('bottom_background_image'); ?>" />
    <?php
}

function bottomcta_link() {
	?>
    	<input type="URL" name="bottom_link" id="bottom_link" value="<?php echo get_option('bottom_link'); ?>" />
    <?php
}

function bottomcta_overlay_color() {
	?>
    	<input type="text" name="bottom_overlay_color" placeholder="Enter Hexcode" id="bottom_overlay_color" value="<?php echo get_option('bottom_overlay_color'); ?>" />
    <?php
}

function dynamics_event_portal() {
    ?>
        <input type="text" name="event_portal" id="event_portal" value="<?php echo get_option('event_portal'); ?>" />
    <?php
}

function display_theme_panel_fields() {
	add_settings_section("section", "Main Settings", null, "theme-options");
    add_settings_section("bottomcta", "Bottom CTA Settings", null, "theme-options");
	
	add_settings_field("accent_color", "Site Accent Color", "main_accent_color", "theme-options", "section");
	add_settings_field("hover_color", "Button Hover Color", "button_hover_color", "theme-options", "section");
    add_settings_field("header_image", "Main Header Image", "main_header_image", "theme-options", "section");
    add_settings_field("event_portal", "Dynamics Event Portal", "dynamics_event_portal", "theme-options", "section");
    
    add_settings_field("bottom_cta_text", "CTA Text", "bottomcta_cta_text", "theme-options", "bottomcta");
    add_settings_field("bottom_body_text", "Body Text", "bottomcta_body_text", "theme-options", "bottomcta");
    add_settings_field("bottom_background_image", "Background Image", "bottomcta_background_image", "theme-options", "bottomcta");
    add_settings_field("bottom_link", "Link", "bottomcta_link", "theme-options", "bottomcta");
    add_settings_field("bottom_overlay_color", "Image Overlay Color", "bottomcta_overlay_color", "theme-options", "bottomcta");

    register_setting("event_settings_all", "accent_color");
    register_setting("event_settings_all", "hover_color");
    register_setting("event_settings_all", "header_image");
    register_setting("event_settings_all", "event_portal");

    register_setting("event_settings_all", "bottom_cta_text");
    register_setting("event_settings_all", "bottom_body_text");
    register_setting("event_settings_all", "bottom_background_image");
    register_setting("event_settings_all", "bottom_link");
    register_setting("event_settings_all", "bottom_overlay_color");
}

add_action("admin_init", "display_theme_panel_fields");

function my_custom_styles(){
 $color = get_option('accent_color');
 $hover_color = get_option('hover_color');
 $bottom_cta_overlay = get_option('bottom_overlay_color');
 $bottom_cta_image = get_option('bottom_background_image');
 echo "
 <style>
 .main-form-style input[type=submit], .main-form-style #customButton, #sign {
    background-color: $color;
  }
 .mobile_menu_bar:before, .et_pb_text_inner a, .et_pb_blurb_description a, #sign:hover, .et_overlay:before, .winners-template-default h1.et_pb_module_header{
    color: $color;
  }
 #sign, .two#sign {
    border: 3px solid $color;
  }
  .et_mobile_menu {
    border-top: 3px solid $color;
  }
  .main-form-style input[type='submit']:hover, .main-form-style #customButton:hover {
    background: $hover_color;
  }
  .title-bottom-border {
    border-bottom: 2px solid $color;
  }
  #cta-row, .animate-cta #cta-row {
    background-color: $bottom_cta_overlay;
  }
  .cta-row-image {
    background-image: url($bottom_cta_image);
  }
  .hover_circle_shedule_after:hover:after, .hover_circle_shedule:hover:before {
    box-shadow: inset 0 0 0px 4px $color;
  }
  .hover_circle_shedule_after:hover:after, .hover_circle_shedule:hover:before {
    box-shadow: inset 0 0 0px 4px $color;
  }
  .woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce .woocommerce-message, body .et_pb_button:hover, .woocommerce a.button.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce button.button.alt.disabled:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page button.button.alt.disabled:hover, .woocommerce input.button.alt:hover, .woocommerce-page input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce #content input.button.alt:hover, .woocommerce-page #content input.button.alt:hover, .woocommerce a.button:hover, .woocommerce-page a.button:hover, .woocommerce button.button:hover, .woocommerce-page button.button:hover, .woocommerce input.button:hover, .woocommerce-page input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce-page #respond input#submit:hover, .woocommerce #content input.button:hover, .woocommerce-page #content input.button:hover {
    background: $color !important;
  }
  #et_search_icon:hover, #top-menu li.current-menu-ancestor>a, #top-menu li.current-menu-item>a, .bottom-nav li.current-menu-item>a, .comment-reply-link, .entry-summary p.price ins, .et-social-icon a:hover, .et_password_protected_form .et_submit_button, .footer-widget h4, .form-submit .et_pb_button, .mobile_menu_bar:after, .mobile_menu_bar:before, .nav-single a, .posted_in a, .woocommerce #content div.product p.price, .woocommerce #content div.product span.price, .woocommerce #content input.button, .woocommerce #content input.button.alt, .woocommerce #content input.button.alt:hover, .woocommerce #content input.button:hover, .woocommerce #respond input#submit, .woocommerce #respond input#submit.alt, .woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit:hover, .woocommerce .star-rating span:before, .woocommerce a.button, .woocommerce a.button.alt, .woocommerce a.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button, .woocommerce button.button.alt, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:hover, .woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce input.button, .woocommerce input.button.alt, .woocommerce input.button.alt:hover, .woocommerce input.button:hover, .woocommerce-page #content div.product p.price, .woocommerce-page #content div.product span.price, .woocommerce-page #content input.button, .woocommerce-page #content input.button.alt, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit, .woocommerce-page #respond input#submit.alt, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page .star-rating span:before, .woocommerce-page a.button, .woocommerce-page a.button.alt, .woocommerce-page a.button.alt:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button, .woocommerce-page button.button.alt, .woocommerce-page button.button.alt.disabled, .woocommerce-page button.button.alt.disabled:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page button.button:hover, .woocommerce-page div.product p.price, .woocommerce-page div.product span.price, .woocommerce-page input.button, .woocommerce-page input.button.alt, .woocommerce-page input.button.alt:hover, .woocommerce-page input.button:hover, .wp-pagenavi a:hover, .wp-pagenavi span.current {
    color: $color !important;
  }
  button.single_add_to_cart_button.button.alt:hover, .woocommerce input#submit:hover, a.button.wc-forward:hover, .woocommerce button.button:hover, a.checkout-button.button.alt.wc-forward:hover, .woocommerce button#place_order:hover {
    color: white !important;
  }
 </style>
    ";
}
add_action('wp_head', 'my_custom_styles', 100);

function remove_menu_items_editor() {
    $role = get_role( 'editor' );
    $role->add_cap( 'manage_options' );
    $role->add_cap( 'edit_theme_options' );
    if ( current_user_can( 'editor' ) ) {
        remove_menu_page( 'edit.php?post_type=project' );
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'admin.php?page=et_divi_options' );
    }
}
add_action( 'admin_init', 'remove_menu_items_editor' );

function custom_post_type() {
	register_post_type( 'winners',
					   array(
						   'labels' => array(
							   'name' => __( 'Winners' ),
							   'singular_name' => __( 'Winner' )
						   ),
						   'query_var' => true,
						   'hierarchical' => true,
						   'taxonomy' => true,
						   'show_ui' => true,
						   'capability_type' => 'page',
						   'menu_icon' => 'dashicons-awards',
						   'rewrite' => array('slug' => 'winners'),
						   'public' => true,
						   'has_archive' => true,
                           'show_in_rest' => true,
						   'supports' => array(
							   'title',
							   'extra-page-post-settings',
							   'tag',
							   'excerpt',
							   'taxonomy',
							   'editor',
							   'image',
							   'description',
							   'thumbnail',
							   'page-attributes',
							   'post-formats',)
					   )
					  );
}

add_action( 'init', 'custom_post_type' );

function winnersyear_custom_taxonomy() {

    $labels = array(
                    'name' => _x( 'Winners Year', 'Winners Year' ),
                    'singular_name' => _x( 'Winners Year', 'Winners Year' ),
                    'search_items' =>  __( 'Search Winners Year' ),
                    'all_items' => __( 'All Winners Years' ),
                    'parent_item' => __( 'Parent Winner Year' ),
                    'parent_item_colon' => __( 'Parent Winner Year:' ),
                    'edit_item' => __( 'Edit Winner Year' ),
                    'update_item' => __( 'Update Winner Year' ),
                    'add_new_item' => __( 'Add New Winner Year' ),
                    'new_item_name' => __( 'New Winner Year' ),
                    'menu_name' => __( 'Winners Year' ),
                    );

    register_taxonomy('winnersyear',array('winners'), array(
                                                    'hierarchical' => true,
                                                    'labels' => $labels,
                                                    'show_ui' => true,
                                                    'show_admin_column' => true,
                                                    'query_var' => true,
                                                    'has_archive' => true,
                                                    'publicly_queryable' => true,
                                                    'show_in_menu' => true,
                                                    'public' => true,
                                                    'rewrite' => array( 'slug' => 'winners-year' ),
                                                    ));
}

add_action( 'init', 'winnersyear_custom_taxonomy' );

  function winner_archive( $atts ) {
                ob_start();
                $custom_terms = get_terms('winnersyear');

                    foreach($custom_terms as $custom_term) {
                        wp_reset_query();
                        $args = array('post_type' => 'winners',
                                      'posts_per_page' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'winnersyear',
                                    'field' => 'slug',
                                    'terms' => $atts['year'],
                                ),
                            ),
                         );
                              $flag = 1;
                             ?>
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                            <script>
                            $( document ).ready(function() {
                            $('#carouselMulti').on('slide.bs.carousel', function (e) {

                                var $e = $(e.relatedTarget);
                                var idx = $e.index();
                                var itemsPerSlide = 3;
                                var totalItems = $('.carousel-item').length;

                                if (idx >= totalItems-(itemsPerSlide-1)) {
                                    var it = itemsPerSlide - (totalItems - idx);
                                    for (var i=0; i<it; i++) {
                                        // append slides to end
                                        if (e.direction=="left") {
                                            $('.carousel-item').eq(i).appendTo('.carousel-inner');
                                        }
                                        else {
                                            $('.carousel-item').eq(0).appendTo('.carousel-inner');
                                        }
                                    }
                                }
                            }); 
                            });
                            $('.carousel-multi').carousel({
                              interval: false
                            });
                            </script>
                            <div class="container-fluid">
                            <div id="carouselMulti" class="carousel carousel-multi slide" data-ride="carousel" data-interval="9000">
                                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                            <?php 
                         $loop = new WP_Query($args);
                         if($loop->have_posts()) {
                            while($loop->have_posts()) : $loop->the_post();
                             
                                $winner_name = get_post_meta(get_the_ID(), 'full_name', TRUE);
                                $winner_position = get_post_meta(get_the_ID(), 'position', TRUE);
                                $image = get_field('image');
                                if($flag == 1) {
                                echo '<div class="carousel-item col-md-4 active">';
                                echo '<a href="'.get_permalink().'">';
                                echo '<img class="img-fluid mx-auto d-block" width="100%" src='.$image['url'].'>';
                                echo '<div>';
                                echo '<p class="name">'.$winner_name.'</p>';
                                echo '<p class="position">'.$winner_position.'</p>';
                                echo '</div>';
                                echo '</a>';
                                echo '</div>';  
                                $flag = 0; 
                                } else {
                                echo '<div class="carousel-item col-md-4">';
                                echo '<a href="'.get_permalink().'">';
                                echo '<img class="img-fluid mx-auto d-block" width="100%" src='.$image['url'].'>';
                                echo '<div>';
                                echo '<p class="name">'.$winner_name.'</p>';
                                echo '<p class="position">'.$winner_position.'</p>';
                                echo '</div>';
                                echo '</a>';
                                echo '</div>';
                                }
                            endwhile;
                         }
                                ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselMulti" role="button" data-slide="prev">
                                    <span class="dashicons dashicons-arrow-left-alt2 text-muted"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next text-faded" href="#carouselMulti" role="button" data-slide="next">
                                    <span class="dashicons dashicons-arrow-right-alt2 text-muted"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                                </div>
                                <?php 
                    }
	  $output = ob_get_clean();
      return $output;
}
add_shortcode('winnerslastyear', 'winner_archive');

function my_scripts() {
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css');
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js', array( 'jquery' ),'',true );
}
add_action( 'wp_enqueue_scripts', 'my_scripts' );

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d9b31bc8a720',
	'title' => 'Winner Fields',
	'fields' => array(
		array(
			'key' => 'field_5d9b323163e9f',
			'label' => 'Full Name',
			'name' => 'full_name',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5d9b32a363ea0',
			'label' => 'Position',
			'name' => 'position',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5d9b32b663ea1',
			'label' => 'Description',
			'name' => 'description',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'visual',
			'toolbar' => 'basic',
			'media_upload' => 0,
			'delay' => 0,
		),
		array(
			'key' => 'field_5d9b32ee63ea2',
			'label' => 'Image',
			'name' => 'image',
			'type' => 'image',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'winners',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'excerpt',
	),
	'active' => true,
	'description' => '',
));

endif;

function bottom_cta() {
    ob_start();
    ?>
    <div id="cta-section" class="et_pb_section et_pb_section_3 et_section_regular">

        <div id="cta-row" class="et_pb_row et_pb_row_1 cta-row-image">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">

                <div class="et_pb_module et_pb_code et_pb_code_0">

                    <div class="et_pb_code_inner">
                        <div class="cta-info">

                            <h3><?php echo get_option('bottom_body_text'); ?></h3>

                            <a href="<?php echo get_option('bottom_link'); ?>" id="sign"><?php echo get_option('bottom_cta_text'); ?></a>

                        </div>
                    </div>
                </div>
                <!-- .et_pb_code -->
            </div>
            <!-- .et_pb_column -->

        </div>
        <!-- .et_pb_row -->

    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('bottomcta', 'bottom_cta');
add_action('template_redirect','redirect_visitor');


function No_product_found() {
    
    if ( WC()->cart->get_cart_contents_count() == 0 ) {
            wc_print_notice( __( 'No product found' ), 'notice' );
            // Change notice text as desired
    }

}

function SuccessfulRegistration() {
    
            wc_print_notice( __( 'Event Registration Successfull' ), 'notice' );
            // Change notice text as desired
    

}

function UnSuccessfulRegistration() {
    
            wc_print_notice( __( 'Event Registration Not Successfull' ), 'notice' );
            // Change notice text as desired
    

}


function redirect_visitor(){


    //add product to woccomerce directly
    if ( is_page( 'cart' ) || is_cart() && strpos($_POST['_wp_http_referer'], 'event-registration')) {

            $attendeeNumber = $_POST['total-registration'];
            if ($attendeeNumber <= 0){
            }else{
                $all_attendees=[];
                for($i=0; $i<$attendeeNumber; $i++){
                    $each_attendess=[];
                    
                    foreach ($_POST as $key => $value) {
                        if( is_array( $value )){
                            $each_attendess[$key] = $_POST[$key][$i];
                        }else{

                               $each_attendess[$key] = $_POST[$key];
                        

                        }
                        
                        }

                array_push($all_attendees,$each_attendess);
                }
            }

            foreach ($all_attendees as $key => $value) {
                $all_attendees[$key]['customRegistrationFieldId'] = array_unique($_POST['customRegistrationFieldId']);
            }

            if(  !empty(  $_POST['passid'] ) ){
                $passid = $_POST['passid'];
            }else{
                echo "<h2> No Pass Id found </h2>";
            }
            
            $params = array(
              'post_type' => 'product',
              'meta_query' => array(
                  array('key' => '_dynamics_product_code', //meta key name here
                      'value' => $passid, 
                      'compare' => '=',
                  )
            ),  
              'posts_per_page' => 1
              
             );
            $wc_query = new WP_Query($params);

            if ($wc_query->have_posts() ) {
                while ( $wc_query->have_posts() ) {
                  $wc_query->the_post(); 


                  $post_id = get_the_ID();    
                 } // end while
            }else{
                add_action( 'woocommerce_check_cart_items', 'No_product_found' );

            } // end if

            // update_post_meta( $post_id, '_regular_price', $_POST["registration_price"] );
            // update_post_meta( $post_id, '_visibility', 'visible' );
            // update_post_meta( $post_id, '_stock_status', 'instock');
            // update_post_meta( $post_id, '_price', $_POST["registration_price"]  );
            if( !empty($post_id )){
                wp_set_object_terms( $post_id, 'simple', 'product_type' );
                WC()->cart->add_to_cart( $post_id );   
            }         


                //also should post the data to crm no matter the registration is inoomplete
                //$value  = json_encode(array("value" => json_encode($_POST))));


                if( !empty( $all_attendees )){
                    
                 $value  = json_encode(array("value" => $all_attendees));

                    $ApiUrl = "https://bonhillplc.azurewebsites.net/api/EventBooking";
                    $bodyRequest = array(
                            "headers"=>array(
                                'Content-Type' => 'application/json'
                            ),
                            "body"=>$value
                    );
                    $api_response = wp_remote_post($ApiUrl,$bodyRequest);

                     // echo '</pre>';
                     // print_r( $api_response['response']);
                     // echo "</pre>";
                        // var_dump( $api_response );

                    if( $api_response['response']['code'] == 200){
                        add_action( 'woocommerce_check_cart_items', 'SuccessfulRegistration' );

                    }else{
                       
                        add_action( 'woocommerce_check_cart_items', 'UnSuccessfulRegistration' );

                    }
               }


        }

}    


