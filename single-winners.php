<?php

get_header();

$show_default_title = get_post_meta( get_the_ID(), '_et_pb_show_title', true );

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">
	<?php
		if ( et_builder_is_product_tour_enabled() ):
			// load fullwidth page in Product Tour mode
			while ( have_posts() ): the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
					<div class="entry-content">
					<?php
						the_content();
					?>
					</div> <!-- .entry-content -->

				</article> <!-- .et_pb_post -->

		<?php endwhile;
		else:
	?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				/**
				 * Fires before the title and post meta on single posts.
				 *
				 * @since 3.18.8
				 */
				do_action( 'et_before_post' );
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'et_pb_post' ); ?>>
					<?php if ( ( 'off' !== $show_default_title && $is_page_builder_used ) || ! $is_page_builder_used ) { ?>
						<div class="et_post_meta_wrapper">
							<h1 class="entry-title"><?php the_title(); ?></h1>

						<?php
							if ( ! post_password_required() ) :

								et_divi_post_meta();

								$thumb = '';

								$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

								$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
								$classtext = 'et_featured_image';
								$titletext = get_the_title();
								$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
								$thumb = $thumbnail["thumb"];

								$post_format = et_pb_post_format();

								if ( 'video' === $post_format && false !== ( $first_video = et_get_first_video() ) ) {
									printf(
										'<div class="et_main_video_container">
											%1$s
										</div>',
										et_core_esc_previously( $first_video )
									);
								} else if ( ! in_array( $post_format, array( 'gallery', 'link', 'quote' ) ) && 'on' === et_get_option( 'divi_thumbnails', 'on' ) && '' !== $thumb ) {
									print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
								} else if ( 'gallery' === $post_format ) {
									et_pb_gallery_images();
								}
							?>

							<?php
								$text_color_class = et_divi_get_post_text_color();

								$inline_style = et_divi_get_post_bg_inline_style();

								switch ( $post_format ) {
									case 'audio' :
										$audio_player = et_pb_get_audio_player();

										if ( $audio_player ) {
											printf(
												'<div class="et_audio_content%1$s"%2$s>
													%3$s
												</div>',
												esc_attr( $text_color_class ),
												et_core_esc_previously( $inline_style ),
												et_core_esc_previously( $audio_player )
											);
										}

										break;
									case 'quote' :
										printf(
											'<div class="et_quote_content%2$s"%3$s>
												%1$s
											</div> <!-- .et_quote_content -->',
											et_core_esc_previously( et_get_blockquote_in_content() ),
											esc_attr( $text_color_class ),
											et_core_esc_previously( $inline_style )
										);

										break;
									case 'link' :
										printf(
											'<div class="et_link_content%3$s"%4$s>
												<a href="%1$s" class="et_link_main_url">%2$s</a>
											</div> <!-- .et_link_content -->',
											esc_url( et_get_link_url() ),
											esc_html( et_get_link_url() ),
											esc_attr( $text_color_class ),
											et_core_esc_previously( $inline_style )
										);

										break;
								}

							endif;
						?>
					</div> <!-- .et_post_meta_wrapper -->
				<?php  } ?>

					<div class="entry-content">
					<?php
                        echo do_shortcode('[Sassy_Social_Share type="floating" left="-10" top="160"]');
                        $image = get_field('image');
						do_action( 'et_before_content' );
                        ?>
                        <div class="et_builder_inner_content et_pb_gutters3">
                            <div class="et_pb_section et_pb_section_0 et_section_regular" style="padding-top: 0px !important">

                                <div class="et_pb_row et_pb_row_0">
                                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">

                                        <div class="et_pb_module et_pb_team_member et_pb_team_member_0 et_pb_bg_layout_light clearfix ">

                                            <div class="et_pb_team_member_image et-waypoint et_pb_animation_off et-animated"><img src="<?php echo $image['url']; ?>" alt="<?php the_field('full_name'); ?>" sizes="((min-width: 0px) and (max-width: 480px)) 480px, (min-width: 481px) 600px, 100vw"></div>
                                            <div class="et_pb_team_member_description">
                                                <h1 class="et_pb_module_header" style="font-weight: 700;"><?php the_field('full_name'); ?></h1>
                                                <p class="et_pb_member_position"><?php the_field('position'); ?></p>
                                                <div>
                                                    <?php the_field('description'); ?>
                                                </div>

                                            </div>
                                            <!-- .et_pb_team_member_description -->
                                        </div>
                                        <!-- .et_pb_team_member -->
                                    </div>
                                    <!-- .et_pb_column -->

                                </div>
                                <!-- .et_pb_row -->

                            </div>
                            <!-- .et_pb_section -->
                        </div>
                        <?php
						wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					</div> <!-- .entry-content -->
					<div class="et_post_meta_wrapper">
					<?php
					if ( et_get_option('divi_468_enable') === 'on' ){
						echo '<div class="et-single-post-ad">';
						if ( et_get_option('divi_468_adsense') !== '' ) echo et_core_intentionally_unescaped( et_core_fix_unclosed_html_tags( et_get_option('divi_468_adsense') ), 'html' );
						else { ?>
							<a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>" alt="468" class="foursixeight" /></a>
				<?php 	}
						echo '</div> <!-- .et-single-post-ad -->';
					}

					/**
					 * Fires after the post content on single posts.
					 *
					 * @since 3.18.8
					 */
					do_action( 'et_after_post' );

						if ( ( comments_open() || get_comments_number() ) && 'on' === et_get_option( 'divi_show_postcomments', 'on' ) ) {
							comments_template( '', true );
						}
					?>
					</div> <!-- .et_post_meta_wrapper -->
				</article> <!-- .et_pb_post -->
                <nav class="navigation winners" role="navigation">
                  <div class="nav-previous"><?php 
                            function Get_Last_permalink(){
                            global $post;
                            $tmp_post = $post;
                            $args = array(
                                'numberposts'     => 1,
                                'offset'          => 0,
                                'orderby'         => 'post_date',
                                'order'           => 'DESC',
                                'post_type'       => 'winners',
                                'post_status'     => 'publish' );
                            $myposts = get_posts( $args );
                            setup_postdata($myposts[0]);
                            $permalink = get_permalink($myposts[0]->ID);
                            $post = $tmp_post;
                            return $permalink;
                        }
                        if( get_adjacent_post(false, '', true) ) { 
                            previous_post_link('%link', '<span class="dashicons dashicons-arrow-left-alt2"></span>');
                        } else { 
                            $first = new WP_Query('posts_per_page=1&order=DESC'); $first->the_post();
                                echo '<a href="' . Get_Last_permalink() . '"><span class="dashicons dashicons-arrow-left-alt2"></span></a>';
                            wp_reset_query();
                        }; 
                      ?></div>
                  <div class="nav-next"><?php 
                            function Get_First_permalink(){
                            global $post;
                            $tmp_post = $post;
                            $args = array(
                                'numberposts'     => 1,
                                'offset'          => 0,
                                'orderby'         => 'post_date',
                                'order'           => 'ASC',
                                'post_type'       => 'winners',
                                'post_status'     => 'publish' );
                            $myposts = get_posts( $args );
                            setup_postdata($myposts[0]);
                            $permalink = get_permalink($myposts[0]->ID);
                            $post = $tmp_post;
                            return $permalink;
                        }
                      if( get_adjacent_post(false, '', false) ) { 
                            next_post_link('%link', '<span class="dashicons dashicons-arrow-right-alt2"></span>');
                        } else { 
                            $last = new WP_Query('posts_per_page=1&order=ASC'); $last->the_post();
                                echo '<a href="' . Get_First_permalink() . '"><span class="dashicons dashicons-arrow-right-alt2"></span></a>';
                            wp_reset_query();
                        }; 
                      ?></div>
                </nav>
                

			<?php endwhile; ?>
	<?php endif; ?>
</div> <!-- #main-content -->

<?php

get_footer();
