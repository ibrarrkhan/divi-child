<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
global $reg_errors;

$reg_errors = new WP_Error;

$request = wp_remote_get( "https://investmentnews.microsoftcrmportals.com/api/events/customregistrationsfields/?readableEventId=w2w");
$events = json_decode( wp_remote_retrieve_body( $request ) );
?>
  <style>

input[type="text"], select{
	padding: 6px 10px;
}
button{
	margin:0 0 25px;
}
.event-holder h2{
	font-size: 20px;
	line-height: 22px;
	font-weight: 700;
	margin: 10px 0;
}
h1{
	font-weight: 700;
	letter-spacing: 2px;
}
.error{
	font-size: 12px;
	line-height: 16px;
	color: #f00;
}
.row-border{
    padding: 0px 15px;
}
#main-footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  z-index: 9;
}
.event-holder{
	padding: 30px 0px;
}
.form-title{
	padding: 10px 0px;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" rel="stylesheet"/>
<div class="container main-form-style" id="repeater-container" style="display:none;">
	<div class="panel panel-default">
		<div class="form-title"><h2>Attendee details <span class="attendee-number"></span> left</h2></div>
		<div class="panel-body">
			<span id="success_result"></span>
			<form id="repeater_form"  action=<?php echo site_url("cart");?> method="post">
				<input type="hidden" name="total-registration" class="total-registration" value="1">
				<?php wp_nonce_field('attendee_details_form', 'verify_attendee_details_form'); ?>
                <div id="repeater">
                	<div class="items" data-group="attendee_details">
                		<div class="item-content">
                			<div class="form-group">

                						<div class="attendee-form-group">
                							<div class="row row-border">
                							
                							<div class="col-md-12">
                                            <div class="row">
                                                    <div class="col-lg-6"><label for="first_name">First Name:</label><input type="text" data-skip-name="true" id="first_name" data-name="first_name[]" name="first_name" required/></div>
                                                    <div class="col-lg-6"><label for="last_name">Last Name:</label><input type="text" data-skip-name="true" id="last_name" data-name="last_name[]" name="last_name" required/></div>
                                                    <div class="col-lg-12"><label for="email">Email Address:</label><input type="email" pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$" data-skip-name="true" id="email" data-name="email[]" name="email" required/></div>
                						      </div>
                							</div>
                							<div class="col-md-12">
                								<br><h5>Do you have any dietary restrictions?(select all that apply) </h5>
                								                							<?php
                							 if( !empty( $events )):
												foreach ( $events as $key => $value ) { ?>
													<?php
													if( $value->type  == 100000001){
														echo '<label>'.$value->text;
														$strippedname =  str_replace(' ', '', $value->customRegistrationFieldId);
														$strippedname =  str_replace("?", '', $strippedname);
														?>
														<!-- we have four input types in crm .this is for input type checkbox -->
														<input type="hidden" class="" data-skip-name="true" id="<?php echo $strippedname; ?> " data-name="<?php echo $strippedname; ?>[]" name="<?php echo $strippedname; ?>[]"  value="false"><input type="checkbox"  onclick="setvaluetohidden( this )"data-skip-name="true" data-name="checkbox[]" name="checkbox[]"  value="false"></label>

														<input type="hidden" data-skip-name="true" name="customRegistrationFieldId[]" value="<?php echo $value->customRegistrationFieldId; ?>">
														<?php
													}
													elseif ( $value->type  == 100000000) {
														$strippedname =  str_replace(' ', '', $value->customRegistrationFieldId);
														$strippedname =  str_replace("?", '', $strippedname);
														//label
														echo '<label>'. $value->text.'</label>';?>
														<input type="text" data-skip-name="true" data-name="<?php echo $strippedname; ?>[]" name="<?php echo $strippedname; ?>[]"></h2><br>
														<input type="hidden"  data-skip-name="true" id="questionType" data-name="questionType[]" name="questionType[]"  value="String">
														<?php
													}
													elseif ($value->type  == 100000003) {
														$strippedname =  str_replace(' ', '', $value->customRegistrationFieldId);
														$strippedname =  str_replace("?", '', $strippedname);
														$items = explode("\n",  $value->choices);

												   ?> 
													  <?php  echo '<label>'.$value->text .'</label>' ;?>
													  <!-- this is for select option type -->
													   <select data-skip-name="true" data-name="<?php echo $strippedname; ?>[]" name= "<?php echo $strippedname; ?>[]"> <?php
													   	foreach ($items as $key => $value) { ?>
													   		
														<option value="<?php echo $value ?>"><?php echo $value ?></option>
														<?php
													   	} ?>
														
														</select>
														<?php
													}elseif ($value->type  == 100000002) {
														$strippedname =  str_replace(' ', '', $value->customRegistrationFieldId);
														$strippedname =  str_replace("?", '', $strippedname);
														$items = explode("\n",  $value->choices);
													 
													 
													   ?> 
													  <?php  echo '<label>'.$value->text .'</label>' ;?>
													  <!-- this is for multiselect option -->
													   <input type="hidden"  data-skip-name="true"   name="<?php echo $strippedname; ?>[]"  value="false"><select multiple data-skip-name="false" id="<?php echo $strippedname; ?>" data-name="<?php echo $strippedname; ?>" name="" onclick ="setmultiselectvalue( this )"> <?php
													   	foreach ($items as $key => $option) { ?>
													   		
														<option value="<?php echo $option ?>"><?php echo $option ?></option>
														<?php
													   	} ?>
														</select>
														<input type="hidden" data-skip-name="true" name="customRegistrationFieldId[]" value="<?php echo $value->customRegistrationFieldId; ?>">

														<?php
													}

											} endif;?>
                							</div>
                							</div>



                						</div>

                        </div>
                    </div>
                </div>
                <div class="repeater-heading">
                	<button class="repeater-add-btn add-attendee"><span class="et-pb-icon icon_plus_alt2"></span> Add another attendee</button>
                </div>
            </div>
            <div class="text-center">
            <input type="hidden" id="passid" name="passid" value="" />
            <input type="hidden" id="eventPassId" name="eventPassId" value="" />
            <input type="hidden" id ="eventId" name="eventId" class="eventId" value="">
                <div class="col-lg-4 offset-lg-8">
            <input type="submit" name="submit" value="SUBMIT" id="customButton" class="btn btn-lg btn-success">
                </div>
            <!-- <input type="button" id="customButton" value="Pay"> -->
            </div>
            
        </form>
    </div>
</div>
</div>
<script>



jQuery(document).ready(function(){
		jQuery("#repeater").createRepeater();

	});


	function setvaluetohidden( id ){


			id.previousSibling.value =  id.checked  ; 


	}

	function setmultiselectvalue( id ){

			var select1 = id;
		    var selected1 = [];
		    for (var i = 0; i < select1.length; i++) {
		        if (select1.options[i].selected) selected1.push(select1.options[i].value);
		    }

    		id.previousSibling.value = selected1.join( ",");


	}



	function form_validation() {
		/* Check the First Name for blank submission*/
		var first_name = document.forms["attendee_details_form"]["first_name"].value;
		var last_name = document.forms["attendee_details_form"]["last_name"].value;
		if (first_name == "" || first_name == null) {
			alert("First name is required");
			return false;
		}
		if (last_name == "" || last_name == null) {
			alert("Last name is required.");
			return false;
		}/* Check the Customer Email for invalid format */
		var email = document.forms["attendee_details_form"]["email"].value;
		var at_position = email.indexOf("@");
		var dot_position = email.lastIndexOf(".");
		if (at_position<1 || dot_position<at_position+2 || dot_position+2>=email.length) {
			alert("Given email address is not valid.");
			return false;
		}
	}



</script>
