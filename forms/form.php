<?php
global $reg_errors;
$reg_errors = new WP_Error;
if($_POST['submit']){
  if ( isset( $_POST['verify_nominee_details'] ) &&
  wp_verify_nonce( $_POST['verify_nominee_details'], 'nominee_details' ) ) {

    // Get data
    $first_name = sanitize_text_field($_POST["first_name"]);
    $last_name = sanitize_text_field($_POST["last_name"]);
    $description = esc_textarea($_POST["description"]);
    $phone = sanitize_text_field($_POST["phone"]);
    $company_name = sanitize_text_field($_POST["company_name"]);
    $job_title = sanitize_text_field($_POST["job_title"]);
    $email = sanitize_email($_POST["email"]);
    $award_category = sanitize_text_field($_POST["award_category"]);
    $self_nominated = $_POST["self_nominated"];

    //Nominee information
    $nominee_first_name = sanitize_text_field($_POST["nominee_first_name"]);
    $nominee_last_name = sanitize_text_field($_POST["nominee_last_name"]);
    $nominee_phone = sanitize_text_field($_POST["nominee_phone"]);
    $nominee_company_name = sanitize_text_field($_POST["nominee_company_name"]);
    $nominee_job_title = sanitize_text_field($_POST["nominee_job_title"]);
    $nominee_email = sanitize_email($_POST["nominee_email"]);

    if ( empty( $first_name ) || empty( $last_name ) || empty( $email ) ) {
      $reg_errors->add('field', 'Required form field is missing');
    }
    if ( empty( $award_category ) ) {
      $reg_errors->add('field', 'No award category');
    }

    $ApiUrl = "https://bonhilleventsapi.azurewebsites.net/api/Nominees";
    $bodyRequest = array(
      "headers"=>array(
        'Content-Type' => 'application/x-www-form-urlencoded'
      ),
      "body"=>array(
        "firstname"=>$first_name,
        "lastname"=>$last_name,
        "email"=>$email,
        "description"=>$description,
        "phone"=>$phone,
        "companyname"=>$company_name,
        "jobtitle"=>$job_title,
        "awardcategory"=>$award_category,
        "selfnominated"=>$self_nominated,
        "nomineefirstname"=>$nominee_first_name,
        "nomineelastname"=>$nominee_last_name,
        "nomineeemail"=>$nominee_email,
        "nomineephone"=>$nominee_phone,
        "nomineecompanyname"=>$nominee_company_name,
        "nomineejobtitle"=>$nominee_job_title,
      )
    );
    $api_response = wp_remote_post($ApiUrl,$bodyRequest);
    // echo '<pre>';
    // print_r( $bodyRequest );die();
    if ( is_wp_error( $api_response ) ) {
      $error_message = $api_response->get_error_message();
      $reg_errors->add('api_response_error', $api_response->get_error_message());
    } else {
      if ($api_response['response']['code']!= 202) {
        $reg_errors->add('api_response_error', $api_response['body']);
      }
      // echo '<pre>';
      // print_r( $api_response );
    }
  } else {
    $reg_errors->add('api_response_error', 'Something is wrong with form submission');
  }
  if ( is_wp_error( $reg_errors ) && $reg_errors->get_error_messages() ) {
    foreach ( $reg_errors->get_error_messages() as $error ) {
        echo '<div class="message">';
        echo '<strong>ERROR</strong>:';
        echo $error . '<br/>';
        echo '</div>';
    }
  }else{
    echo '<div class="message">';
    echo '<strong>Success: Successfully submitted</strong>';
    echo '</div>';

  }
}
?>
<div class="form-title"></div>
<form id="nominee_details_form" name="nominee_details" method="POST" onsubmit="return form_validation()" action="#" style="display:none;" >
  First Name: <input type="text" id="first_name" name="first_name" required/>
  Last Name: <input type="text" id="last_name" name="last_name" required/>
  Email: <input type="text" id="email" name="email" required/><br />

  Phone: <input type="text" id="phone" name="phone"/>
  Company Name: <input type="text" id="company_name" name="company_name"/>
  Job Title: <input type="text" id="job_title" name="job_title"/>
  <input type="hidden" id="award_category" name="award_category" value=""/>
   Self Nominated: <input type="radio" name="self_nominated" value="true" checked="checked">Yes <input type="radio" name="self_nominated" value="false">No<br />
   <div class="nominee-form" style="display:none;">
      <h2>Please enter nominee information</h2>
      Nominee First Name: <input type="text" id="nominee_first_name" name="nominee_first_name"/>
      Nominee Last Name: <input type="text" id="nominee_last_name" name="nominee_last_name"/>
      Nominee Email: <input type="text" id="nominee_email" name="nominee_email"/><br />
      Nominee Phone: <input type="text" id="nominee_phone" name="nominee_phone"/>
      Nominee Company Name: <input type="text" id="nominee_company_name" name="nominee_company_name"/>
      Nominee Job Title: <input type="text" id="nominee_job_title" name="nominee_job_title"/>
  </div>
  Description: <textarea rows="4" cols="50" id="description" name="description"></textarea>
  <?php wp_nonce_field('nominee_details', 'verify_nominee_details'); ?>
  <input type="submit" value="Submit" name="submit"/>
</form>

<script type="text/javascript">
jQuery(document).ready(function(){
  var selfNominee = jQuery('input[type=radio][name=self_nominated]').val();
  if (selfNominee=="false"){
    jQuery('.nominee-form').show();
  }
  jQuery('input[type=radio][name=self_nominated]').change(function() {
    if (this.value == "false") {
      jQuery('.nominee-form').show();
    }
    else {
      jQuery('.nominee-form').hide();
    }
});

})
function form_validation() {
/* Check the First Name for blank submission*/
  var first_name = document.forms["nominee_details"]["first_name"].value;
  var last_name = document.forms["nominee_details"]["last_name"].value;
  if (first_name == "" || first_name == null) {
    alert("First name is required");
    return false;
  }
  if (last_name == "" || last_name == null) {
    alert("Last name is required.");
    return false;
  }/* Check the Customer Email for invalid format */
  var email = document.forms["nominee_details"]["email"].value;
  var at_position = email.indexOf("@");
  var dot_position = email.lastIndexOf(".");
  if (at_position<1 || dot_position<at_position+2 || dot_position+2>=email.length) {
    alert("Given email address is not valid.");
    return false;
  }
}
</script>
