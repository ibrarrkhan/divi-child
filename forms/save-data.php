<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once( '../../../../wp-load.php' );
if ( isset( $_POST['verify_nominee_details'] ) &&
wp_verify_nonce( $_POST['verify_nominee_details'], 'nominee_details' ) ) {
      // Get data
  $first_name = sanitize_text_field($_POST["first_name"]);
  $last_name = sanitize_text_field($_POST["last_name"]);
  $description = esc_textarea($_POST["description"]);
  $phone = sanitize_text_field($_POST["phone"]);
  $company_name = sanitize_text_field($_POST["company_name"]);
  $job_title = sanitize_text_field($_POST["job_title"]);
  $email = sanitize_email($_POST["email"]);
  $award_category = sanitize_text_field($_POST["award_category"]);
  $self_nominated = $_POST["self_nominated"];


  $ApiUrl = "https://bonhilleventsapi.azurewebsites.net/api/Nominees";
  $bodyRequest = array(
    "headers"=>array(
      'Content-Type' => 'application/x-www-form-urlencoded'
    ),
    "body"=>array(
      "firstname"=>$first_name,
      "lastname"=>$last_name,
      "email"=>$email,
      "description"=>$description,
      "nomineephone"=>$phone,
      "companyname"=>$company_name,
      "jobtitle"=>$job_title,
      "awardcategory"=>$award_category,
      "selfnominated"=>$self_nominated
    )
  );
  $api_response = wp_remote_post($ApiUrl,$bodyRequest);
  if ( is_wp_error( $api_response ) ) {
    $error_message = $api_response->get_error_message();
    echo "Something went wrong: $error_message";
  } else {
    echo 'Response:<pre>';
    print_r( $api_response );
    echo '</pre>';
    return $api_response['body'];
  }
} else {
  echo 'Something is worng with form submission';
}

?>
