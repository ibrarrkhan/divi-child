jQuery.fn.extend({
  createRepeater: function() {
    var addItem = function(items, key) {
      var itemContent = items;
      var group = itemContent.data("group");
      var item = itemContent;


      var input = item.find("input,select");
      input.each(function(index, el) {
        var attrName = jQuery(el).data("name");
        var skipName = jQuery(el).data("skip-name");
        if (skipName != true) {
         // jQuery(el).attr("name", group + + "[" key + "]" + attrName);
         // jQuery(el).attr("name", attrName + key+'[]');
     } else {
       if (attrName != "undefined") {
         jQuery(el).attr("name", attrName);
       }
     }
 });
      var itemClone = items;

      /* Handling remove btn */
      var removeButton = itemClone.find(".remove-btn");
      removeButton =
      '<button class="remove-attendee"><span  id="remove-btn" class="remove-btn-button et-pb-icon icon_minus_alt2"></span> Remove attendee</button>';
      if (key == 0) {
        // removeButton.attr("disabled", true);
        removeButton = "";
    } else {
        // removeButton.attr("disabled", false);
        removeButton =
        '<button class="remove-attendee"><span  id="remove-btn" class="remove-btn-button et-pb-icon icon_minus_alt2"></span> Remove attendee</button>';
    }

    jQuery(
      "<div class='items'>" + removeButton + itemClone.html() + "<div/>"
      ).appendTo(repeater);
};
/* find elements */
var repeater = this;
var items = repeater.find(".items");
var key = 0;
var addButton = repeater.find(".repeater-add-btn");
    //var removeButton = repeater.find("#remove-btn");
    var newItem = items;

    if (key == 0) {
      items.remove();
      addItem(newItem, key);
    }
     var attendeeNumber = jQuery(".attendee-number").html();

    /* handle click and add items */
    addButton.on("click", function() {
      var attendeeNumber = jQuery(".attendee-number").html();
      remainingNumber = attendeeNumber - 1;

      if (remainingNumber == 0) {
        addButton.attr("disabled", true);
      }
      jQuery(".attendee-number").html(remainingNumber);
      var registrationNumber = jQuery(".total-registration").val();
      // alert( registrationNumber);
      totalNumber = parseInt(registrationNumber) + 1;
      jQuery(".total-registration").val(totalNumber);
      key++;
      addItem(newItem, key);
    });

    /* handle click and remove items */
    jQuery("body").on("click", ".remove-btn-button", function() {

   

      var attendeeNumber = jQuery(".attendee-number").html();
      remainingNumber = parseInt(attendeeNumber) + 1;
      jQuery(".attendee-number").html(remainingNumber);
      var registrationNumber = jQuery(".total-registration").val();
      totalNumber = parseInt(registrationNumber) - 1;
      jQuery(".total-registration").val(totalNumber);
      if (remainingNumber > 0) {
        addButton.attr("disabled", false);
      }
      jQuery(this)
      .parents(".items")
      .remove();
  });
}
});
